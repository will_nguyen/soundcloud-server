ActiveRecord::Base.transaction do
  # Default play list for crawler
  User.create!([
                 { name: :admin, role: :admin, email: 'admin@gmail.com', password: :password, password_confirmation: :password },
                 { name: :editor, role: :editor, email: 'editor@gmail.com', password: :password, password_confirmation: :password },
                 { name: :contributor, role: :contributor, email: 'contributor@gmail.com', password: :password, password_confirmation: :password }
               ])
  Playlist.find_or_create_by(title: :uncategory, sharing: :public, playlist_type: :unknown)
end