class AddReferences < ActiveRecord::Migration[5.1]
  def change
    add_reference :playlists, :genre, foreign_key: true
    add_reference :tracks, :genre, foreign_key: true
  end
end
