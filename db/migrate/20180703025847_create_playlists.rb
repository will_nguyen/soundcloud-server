class CreatePlaylists < ActiveRecord::Migration[5.1]
  def change
    create_table :playlists do |t|
      t.string :title
      t.string :artwork_url
      t.string :description
      t.integer :duration
      t.string :sharing
      t.string :permalink_url
      t.boolean :streamable, default: true
      t.boolean :downloadable, default: true
      t.string :playlist_type # "recording"

      t.timestamps
    end
  end
end
