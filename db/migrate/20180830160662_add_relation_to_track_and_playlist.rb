class AddRelationToTrackAndPlaylist < ActiveRecord::Migration[5.1]
  def change
    create_join_table :playlists, :tracks do |t|
      t.index :playlist_id
      t.index :track_id
    end
  end
end
