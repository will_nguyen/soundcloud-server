class CreateTracks < ActiveRecord::Migration[5.1]
  def change
    create_table :tracks do |t|
      t.string :title
      t.string :state                 # encoding state	"finished"
      t.string :sharing               # public/private sharing
      t.string :source
      t.string :tag_list
      t.string :description           # duration in milliseconds
      t.integer :duration

      t.boolean :commentable, default: true
      t.boolean :streamable, default: true
      t.boolean :downloadable, default: true

      t.string :track_type            # "recording"
      t.string :original_format       # "wav", "mp3" ...
      t.string :original_content_size # byte "10211857"
      t.string :permalink_url         # "will-sounds"
      t.string :artwork_url           # URL to a JPEG image
      t.string :waveform_url          # URL to PNG waveform image
      t.string :stream_url            # link to 128kbs mp3 stream
      t.string :download_url

      t.integer :playback_count       # track play count
      t.integer :download_count
      t.integer :favoritings_count    # track favoriting count
      t.integer :comment_count

      t.timestamps
    end
  end
end
