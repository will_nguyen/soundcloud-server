source 'https://rubygems.org'
ruby '2.5.1'
git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


gem 'rails', '~> 5.1.6'
gem 'pg'
gem 'puma', '~> 3.7'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'factory_bot_rails', '~> 4.8'
  gem 'faker'
  gem 'cucumber', '~> 3.0.0'
  gem 'cucumber-rails', '~> 1.6.0', require: false
  gem 'database_cleaner', '~> 1.6'
  gem 'rack_session_access'
  gem 'rspec', '~> 3.7.0'
  gem 'rspec-rails', '~> 3.7.2', group: :development
  gem 'rspec-activemodel-mocks', '~> 1.0.3', git: 'https://github.com/rspec/rspec-activemodel-mocks'
  gem 'capybara', '~> 3.0.0'
  gem 'capybara-screenshot', '~> 1.0.17'
  gem 'capybara-select2', git: 'https://github.com/goodwill/capybara-select2', ref: '585192e'
  gem 'chromedriver-helper', '~> 1.2.0'
  gem 'selenium-webdriver', '~> 3.11'
  gem 'shoulda-matchers', '~> 3.1', require: nil
  gem 'guard-rspec', require: false
  gem 'fuubar'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  gem 'capistrano', '~> 3.10.1', require: false
  gem 'capistrano-rvm',     require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma',   require: false
  gem 'capistrano-rbenv', '~> 2.0',   require: false
  gem 'capistrano3-monit',   require: false
  gem 'capistrano-rails-console', require: false
  gem 'capistrano-sidekiq', github: 'seuros/capistrano-sidekiq'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'airbrake', '~> 5.1.0', require: false
gem 'rabl', '~> 0.13.0'
gem 'bcrypt', '~> 3.1.6'
gem 'devise_token_auth'
gem 'enumerize'
gem 'jbuilder' # returns json as default
gem 'dotenv-rails'
gem 'sidekiq'
gem 'whenever', require: false
gem 'nokogiri'
gem 'rack-cors', require: 'rack/cors'
gem 'pagy'