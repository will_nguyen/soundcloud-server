
namespace :sounds do
  desc 'getting tracks on soundcloud.com'
  task sync: :environment do
    print 'Synchronizing...'
    waiting = Thread.new { loop { sleep 1 ;print '.' } }
    CrawlerServices::SoundCloud::Crawler.new.sync!
    waiting.exit
    print '-- done.'
  end
end