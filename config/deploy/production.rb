server '45.63.87.250', user: 'deployer', roles: %w{web app}

role :app, %w{deployer@45.63.87.250}
role :web, %w{deployer@45.63.87.250}
role :db,  %w{deployer@45.63.87.250}

set :rails_env, 'production'
set :branch, 'master'
set :stage, 'production'

append :linked_files, ".env.production"

