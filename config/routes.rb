Rails.application.routes.draw do
  resources :genres
  resources :playlists
  resources :tracks
  mount_devise_token_auth_for 'User', at: '/auth'
end
