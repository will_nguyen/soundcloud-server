module CrawlerServices
  module SoundCloud
    class ScTrack
      attr_accessor :title, :artwork_url, :permalink_url, :source

      def initialize(args)
        @title = args[:title].presence
        @artwork_url = args[:artwork_url].presence
        @permalink_url = args[:permalink_url].presence
        @source = args[:source].presence
      end
    end
  end
end