module CrawlerServices
  module SoundCloud
    class Base
      include Channel

      HOST = 'soundcloud'
      DOMAIN = 'https://soundcloud.com'
      CSS_TRACK_PATH  = 'article.audible a'

      def initialize
        @editor = User.editor.first
        @sites = PERMA_CHANNEL.collect { |perma_channel| Site.new(editor, perma_channel) }
      end

      def sync!
        raise NotImplementedError
      end

      private

      attr_reader :sites, :editor

    end
  end
end