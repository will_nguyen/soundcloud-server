require 'nokogiri'
require 'open-uri'

module CrawlerServices
  module SoundCloud
    class Site < Base
      include Concern

      attr_reader :editor, :url, :page, :source

      def initialize(editor, perma_channel)
        @editor = editor
        @url = "#{DOMAIN}/#{perma_channel}"
        @source = "#{HOST}::#{perma_channel}"
        @page = Nokogiri::HTML(open(url))
      end

      def update_new_tracks
        collect_tracks.each { |track| editor.tracks.build(track) if Track.find_by(track).nil? }
        editor.save!
      end

      def collect_tracks
        page.css(CSS_TRACK_PATH).each_with_object([]) do |a_tag, tracks|
          next if url.end_with?(a_tag['href'])
          track = ScTrack.new({ permalink_url: "#{DOMAIN}#{a_tag['href']}" })
          track.artwork_url = find_artwork_url(Nokogiri::HTML(open(track.permalink_url)))
          track.title = a_tag.content
          track.source = source
          tracks.push(to_h(track))
        end
      end
    end
  end
end