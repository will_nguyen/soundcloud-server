module CrawlerServices
  module SoundCloud
    module Concern
      def to_h(track)
        {
          title: track.title,
          permalink_url: track.permalink_url,
          artwork_url: track.artwork_url,
          source: track.source
        }
      end

      def is_artwork?(img_src)
        img_src.split('/').last.start_with?('artworks')
      end

      def find_artwork_url(html_page)
        artwork_url = html_page.css('img').each do |img_tag|
          if is_artwork?(img_tag['src'])
            return img_tag['src']
            break
          end
        end
        artwork_url
      end

      def get_page(url)
        Nokogiri::HTML(open(url))
      end
    end
  end
end