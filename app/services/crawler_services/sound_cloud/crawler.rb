module CrawlerServices
  module SoundCloud
    class Crawler < Base

      def initialize
        super
      end

      def sync!
        sites.each(&:update_new_tracks)
      end
    end
  end
end