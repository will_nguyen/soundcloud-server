class Playlist < ApplicationRecord
  has_and_belongs_to_many :tracks
  extend Enumerize

  validates :title, presence: true

  enumerize :sharing, in: [:public, :private], default: :public
  enumerize :playlist_type, in: [:recording, :unknown], default: :unknown
end
