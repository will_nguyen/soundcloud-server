class Track < ApplicationRecord
  belongs_to :user
  has_and_belongs_to_many :playlists
  extend Enumerize

  validates :title, presence: true
  enumerize :sharing, in: [:public, :private], default: :public
  enumerize :state, in: [:uploading, :finished]
end
