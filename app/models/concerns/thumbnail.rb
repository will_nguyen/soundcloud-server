module Thumbnail
  extend ActiveSupport::Concern

  included do
    attr_accessor :thumb_dir
    after_initialize :generate_url
    before_create :set_thumb_dir, :make_thumnails
  end

  def make_thumnails
    self.thumb = process_image.each_with_object({}) do |(size, file), thumbs|
      thumbs[size] = upload_thumbs(file)
    end
  end

  def thumb_url(size = 'small')
    self.class.bucket.object(self.thumb[size]).public_url unless self.thumb.blank?
  end

private

  def process_image
    sizes.inject({}) do |hash, (name, (width, height))|
      thumbnail = ImageProcessing::MiniMagick
                    .source(self.file_content)
                    .loader(define: { jpeg: { size: "#{width*2}x#{height*2}" } })
                    .resize_to_limit!(width, height)

      hash.merge!(name => thumbnail)
    end
  end

  def upload_thumbs(tmp_file)
    self.class.bucket.put_object(key: thumb_path(tmp_file), body: tmp_file, acl: 'public-read').key
  end

  def sizes
    {
      # 'large' => [800, 800],
      # 'medium' => [500, 500],
      'small' => [300, 300],
      'icon' => [150, 150]
    }
  end

  def generate_url
    sizes.keys.each do |size|
      define_singleton_method("#{size}_thumb_url") do
        self.class.bucket.object(self.thumb[size]).public_url unless self.thumb.blank?
      end
    end
  end

  def thumb_path(tmp_file)
    time = DateServices::Format.new(Time.now).year_month_day_time
    "#{self.thumb_dir}/thumb-#{time}#{File.extname(tmp_file)}"
  end

  def set_thumb_dir
    self.thumb_dir = "images/thumbnails"
  end
end