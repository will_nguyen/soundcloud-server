module Concerns
  module RolesConcerns
    extend ActiveSupport::Concern

    included do
      scope :admin, -> { where(role: :admin) }
      scope :editor, -> { where(role: :editor) }
      scope :contributor, -> { where(role: :contributor) }
    end


  end
end