class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  has_many :tracks

  extend Enumerize
  include Concerns::RolesConcerns

  # Editor: can access to all the content, for content management.
  # Contributor: can write, edit, and delete their own unpublished posts, but their content must be reviewed and published by an Admin or Editor.
  enumerize :role, in: [:admin, :editor, :contributor]

  def destroy_expired_tokens
    return if self.tokens == '{}'
    super
  end
end
