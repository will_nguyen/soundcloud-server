collection @playlists
attribute :title, :artwork_url, :description, :duration, :permalink, :permalink_url, :streamable
node :kind do
    'playlist'
end
child :tracks do |track|
    extends 'tracks/index.json.rabl'
end