class TracksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_track, only: [:show, :update, :destroy]
  respond_to :json

  def index
    @pagy, @tracks = pagy(Track.includes(:user).order(created_at: :desc))
  end

  def show
  end

  def create
    @track = Track.new(track_params)

    if @track.save
      render json: @track, status: :created, location: @track
    else
      render json: @track.errors, status: :unprocessable_entity
    end
  end

  def update
    if @track.update(track_params)
      render json: @track
    else
      render json: @track.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @track.destroy
  end

  private
    def set_track
      @track = Track.find(params[:id])
    end

    def track_params
      params.require(:track).permit(:user_id, :duration, :commentable, :state, :sharing, :tag_list, :permalink, :description, :streamable, :downloadable, :genre, :release, :purchase_url, :label_id, :label_name, :isrc, :video_url, :track_type, :key_signature, :bpm, :title, :release_year, :release_month, :release_day, :original_format, :original_content_size, :license, :uri, :permalink_url, :artwork_url, :waveform_url, :user, :stream_url, :download_url, :playback_count, :download_count, :favoritings_count, :comment_count, :attachments_uri)
    end
end
