class ApplicationController < ActionController::API
  # rails API doesn't render view by default
  include DeviseTokenAuth::Concerns::SetUserByToken
  include ActionView::Rendering
  include Pagy::Backend
  respond_to :json

  def error_400
    render json: { message: '400: Bad request' }, status: 400
  end

  def error_404
    render json: { message: '404: Page/Item not found' }, status: 404
  end

  def error_500
    render json: { message: '500: Unexpected error' }, status: 500
  end

  def render_2xx(status = 200, data)
    render json: {
        success: true,
        status: status,
        data: data
    }
  end

  def error_4xx(status = 200, message)
    render json: {
        success: false,
        status: status,
        message: message
    }, status: status
  end
end
