## Common Issue needs to pay attention:
- secret_key_base and devise_secret_key need to be predefined.
- set up:
    - shared/.env.production and 
    - shared/puma.rb
    - shared/config/secrets.yml 
    
## Development
### Guard
With Guard we can automate our test suite running only the tests related to the updated spec, model, controller or file we are working at.
```apple js
bundle exec guard init rspec
```
Run this command so that every time we make changes, relevant tests will be run automatically.
```apple js
bundle exec guard
```
Using Fuubar to make the progress bar while tests are running.

### Shoulda Matchers
Adding more matchers supporting rails's functionality.
[Instruction 1](https://github.com/thoughtbot/shoulda-matchers)
[Instruction 2](http://matchers.shoulda.io/)
