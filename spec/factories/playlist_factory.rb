FactoryBot.define do
	factory :playlist do
		title Faker::Artist.name
		artwork_url Faker::Avatar.image
		description Faker::Lorem.paragraph
		duration 1000
		permalink 'link'
		permalink_url 'url'
		uri ''

		trait :recording do
			playlist_type :recording
		end

		trait :unknown do
			playlist_type :unknown
		end

		trait :no_permalink do
			permalink ''
		end

		trait :no_permalink_url do
			permalink_url ''
		end

		factory :recording_playlist, traits: [:recording]
		factory :unknown_playlist, traits: [:unknown]

		# invalid
		factory :no_permalink_and_url, traits: [:no_permalink, :no_permalink_url]
	end
end