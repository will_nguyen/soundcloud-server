FactoryBot.define do
  factory :genre do
    title Faker::Lorem.word

		trait :having_title do
			title Faker::Lorem.word
		end

		trait :no_title do
				title ''
		end
  end
end