FactoryBot.define do 
	factory :user do
		nickname Faker::Internet.user_name
		email Faker::Internet.email
		password 'password'
		password_confirmation 'password'
    role :editor
	end
end