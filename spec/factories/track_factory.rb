FactoryBot.define do
	factory :track do
		title Faker::Lorem.sentence
		state 'finished'
		sharing 'public'
		description Faker::Lorem.paragraph
		duration 1000
	end
end