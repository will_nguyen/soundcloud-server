require 'rails_helper'

# This test I think no need to do, because we have controller tests which cover these cases.
RSpec.describe "Tracks", type: :request do
  include_context :request_setup

  describe "GET /tracks" do
    it "works" do
      get tracks_path(json)
      expect(response).to have_http_status(200)
    end
  end
end
