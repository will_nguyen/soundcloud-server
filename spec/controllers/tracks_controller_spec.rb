require 'rails_helper'

RSpec.describe TracksController, type: :controller do

  include_context :project_setup
  include_context :controller_setup

  let(:valid_attributes) {
    { title: Faker::Lorem.sentence, state: :uploading, user_id: user.id }
  }

  let(:invalid_attributes) {
    { title: Faker::Lorem.sentence, state: :new }
  }

  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      user.tracks.build(valid_attributes)
      user.save!
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      track = Track.create! valid_attributes
      get :show, params: {id: track.to_param}, session: valid_session
      expect(response).to be_success
    end
  end


  describe "POST #create" do
    context "with valid params" do
      it "creates a new Track" do
        expect {
          post :create, params: {track: valid_attributes}, session: valid_session
        }.to change(Track, :count).by(1)
      end

      it "renders a JSON response with the new track" do

        post :create, params: {track: valid_attributes}, session: valid_session
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(track_url(Track.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new track" do

        post :create, params: {track: invalid_attributes}, session: valid_session
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        { sharing: :public }
      }

      it "updates the requested track" do
        track = Track.create! valid_attributes
        put :update, params: {id: track.to_param, track: new_attributes}, session: valid_session
        track.reload
        expect(track.sharing).to include('public')
      end

      it "renders a JSON response with the track" do
        track = Track.create! valid_attributes

        put :update, params: {id: track.to_param, track: valid_attributes}, session: valid_session
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the track" do
        track = Track.create! valid_attributes

        put :update, params: {id: track.to_param, track: invalid_attributes}, session: valid_session
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested track" do
      track = Track.create! valid_attributes
      expect {
        delete :destroy, params: {id: track.to_param}, session: valid_session
      }.to change(Track, :count).by(-1)
    end
  end

end
