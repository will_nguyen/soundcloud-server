RSpec.shared_context :project_setup do
  let!(:user) { FactoryBot.create(:user) }
end

RSpec.shared_context :controller_setup do
  before :each do
    request.headers["accept"] = 'application/json'
  end
end

RSpec.shared_context :request_setup do
  let(:json) { { format: 'json' } }
end