require 'rails_helper'

describe :Crawler, type: :integration do
  let!(:user) { FactoryBot.create(:user, role: :editor) }
  let(:crawler) { CrawlerServices::SoundCloud::Crawler.new }

  describe '#crawler' do
    before(:each) do
      crawler.sync!
    end

    it 'has number of tracks is larger than 5' do
      expect(Track.count).to be > 5
    end
  end
end