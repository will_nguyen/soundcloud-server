require 'rails_helper'

RSpec.describe Track, type: :model do
  include_context :project_setup

  context 'valid track' do
    it 'returns a valid instance' do
      track = FactoryBot.create(:track, user_id: user.id)
      expect(track).to be_valid
    end
  end

  context 'invalid track' do
    it 'is invalid because lacking of title' do
      track = FactoryBot.build(:track, title: '', user_id: user.id)
      track.valid?
      expect(track.errors[:title]).to include("can't be blank")
    end

    it 'is invalid because of invalid sharing' do
      track = FactoryBot.build(:track, sharing: :hidden, user_id: user.id)
      track.valid?
      expect(track.errors[:sharing]).to include('is not included in the list')
    end

    it 'is invalid because of invalid state' do
      track = FactoryBot.build(:track, state: :died, user_id: user.id)
      track.valid?
      expect(track.errors[:state]).to include('is not included in the list')
    end
  end

  describe '::Track' do
    it { expect belong_to(:user) }
    it { expect enumerize(:sharing).in(:public, :private) }
    it { expect enumerize(:state).in(:uploading, :finished) }
    it { expect validate_presence_of(:title) }
  end
end
